package com.company;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;




public class Main {

    //final static String FILE_NAME = "F:\\Escritorio\\easey.in";


    //final static String FILE_NAME = "F:\\Escritorio\\learn_and_teach.in";
    final static String FILE_NAME = "F:\\Escritorio\\right_angle.in";
    //final static String FILE_NAME = "F:\\Escritorio\\logo.in";
    final static String OUTPUT_FILE_NAME = "F:\\Escritorio\\output.in";
    final static Charset ENCODING = StandardCharsets.US_ASCII;

    public static void main(String[] args) throws IOException {
        pictureManager manager;
        manager = new pictureManager();
        manager.readLargerTextFile(FILE_NAME);

        ArrayList<String> aLines = new ArrayList<>();





        for(int i = 0; i<manager.getN();i++) {
            for (int e = 0; e < manager.getM(); e++) {
                if(manager.picture[i][e] == '#'){


                    int hl = manager.findLongesHorizontalLine(i,e);
                    int vl = manager.fingLongesVerticalLine(i,e);
                   //int aux= manager.findLongesSquare(i,e);
                   //int sq =  (2*aux+1)*(2*aux+1);
                    int aux= manager.findLongesSquareV3(i,e);
                    int sq =  (2*aux+1)*(2*aux+1);


                    if(hl>=vl && hl>sq){
                      aLines.add(manager.getInstruccionNumber(),"PAINT_LINE "+i+" "+e+" "+i+" "+(e+hl));
                      manager.paint(i,e,i,e+hl,'*');
                  }else if (vl>hl && vl>=sq){
                      aLines.add(manager.getInstruccionNumber(),"PAINT_LINE "+i+" "+e+" "+(i+vl)+" "+e);
                      manager.paint(i,e,i+vl,e,'*');


                  }else if(sq>=hl && sq>vl){
                     // aLines.add(InstruccionNumber,"PAINT_SQUARE "+i+" "+e+" "+aux);
                    //  manager.paint(i-aux,e-aux,i+aux,e+aux);
                        aLines.add(manager.getInstruccionNumber(),"PAINT_SQUARE "+(i+aux)+" "+(e+aux)+" "+aux);
                        manager.paint(i,e,i+(2*(aux)),e+(2*(aux)),'*');


                  }

                }
            }
        }


        aLines.add(0,manager.getInstruccionNumber()+"");

        //esto solo se aplica en el caso de usar el V3 del detector de cuadrados
        System.out.println("ERASE:"+(manager.getErr().size()/2));
        for(int i = 0; i<manager.getErr().size();i+=2){
            manager.setInstruccionNumber(1);
            aLines.add(manager.getInstruccionNumber(),"ERASE_CELL"+(manager.getErr().get(i))+" "+(manager.getErr().get(i+1)));
            manager.erasePoint(manager.getErr().get(i),manager.getErr().get(i+1));
        }
      //  manager.PrintPicture();
       // manager.PrintPictureCopy();

        Path path = Paths.get(OUTPUT_FILE_NAME);
        Files.write(path, aLines, ENCODING);

       // manager.PrintPicture();
       // manager.PrintPictureCopy();


        manager.compareResult();
        System.out.println("Puntos"+((manager.getN()*manager.getM())-manager.getInstruccionNumber())+"   Iteraciones"+manager.getInstruccionNumber());




    }








}
