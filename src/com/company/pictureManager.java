package com.company;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/**
 * Created by Pablo on 06/02/2016.
 */
public class pictureManager {

    final static Charset ENCODING = StandardCharsets.UTF_8;
    private int  N;
    private int M;
    public char[][]picture;
    public char[][]pictureCopy;
    private  List<Integer> err = new ArrayList<Integer>();
    private int InstruccionNumber;

    public  pictureManager(){
        this.N = 0;
        this.M = 0;
        this.InstruccionNumber = 0;
    }

    public void readLargerTextFile(String aFileName) throws IOException {
       boolean first = true;
        int cont= 0;
        Path path = Paths.get(aFileName);
        try (Scanner scanner =  new Scanner(path, ENCODING.name())){

            while (scanner.hasNextLine()){


                if(first){
                    GetMatrixFromString(scanner.nextLine());
                    first=false;
                }
                else{

                   AddLineToMatrix(cont,scanner.nextLine());
                    cont ++;
                }
                //process each line in some way

            }


        }

    }

    private void GetMatrixFromString(String str){

        String[] parts = str.split(" ");
        this.N = Integer.parseInt(parts[0]);
        this.M = Integer.parseInt(parts[1]);
        System.out.println("N:"+this.N);
        System.out.println("M:"+this.M);
        picture = new char[N][M];
        pictureCopy = new char[N][M];

    }

    public void PrintPicture(){
        System.out.println("Pintando Matriz");

        for(int i = 0; i<this.N;i++){

            for(int e = 0; e<this.M;e++){
                System.out.print(picture[i][e]);
            }

            System.out.print('\n');
        }
    }
    public void PrintPictureCopy(){
        System.out.println("Pintando Matriz Cpia");

        for(int i = 0; i<this.N;i++){

            for(int e = 0; e<this.M;e++){
                System.out.print(pictureCopy[i][e]);
            }

            System.out.print('\n');
        }
    }

    private void AddLineToMatrix(int row,String s){
        for(int i= 0; i<M;i++){

            picture[row][i] =s.charAt(i);
            pictureCopy[row][i] =s.charAt(i);

        }
    }

    public int findLongesHorizontalLine(int R,int C){
        int start = R;
        int end = C;
        do{
           end ++;
        }while(end < this.M && picture[start][end] ==  '#');

        end = end-1;
     //   System.out.println("Inicio de la linea"+R+"-"+C);
     //   System.out.println("Fin de la linea"+start+"-"+(end));

        return ((end - C));
    }


    public int getN() {
        return N;
    }

    public int getM() {
        return M;
    }

    public int fingLongesVerticalLine(int R, int C) {
        int start = R;
        int end = C;

        do{
            start ++;
        } while(start < this.N && picture[start][end] ==  '#');

        start= start-1;


       // System.out.println("Inicio de la linea"+R+"-"+C);
       // System.out.println("Fin de la linea"+start+"-"+(end));

        return ((start - R));
    }

    public int findLongesSquare(int R, int C) {
        int S  = 1;

        boolean finish = false;



        while(!finish){
            boolean success = true;

            int startx = R-S;
            int starty = C-S;
            int dim = (2*S)+1;

           // System.out.println("Voy desde"+startx+"hasta"+starty+"");
          //  System.out.println("Voy desde"+(R+dim)+"hasta"+(C+dim));
           // System.out.println("--------------------------");


            for(int i = startx; i<=R+dim;i++) {
                for (int e = starty; e <= C+dim; e++) {
                    if(!isMatrixPoint(i,e) || picture[i][e] != '#'){
                        success =false;
                    }
                }
            }

            if(success){
                S++;
            }else{
                finish = true;
            }
        }

        return S-1;
    }

    public int findLongesSquareV2(int R, int C) {
        int S  = 1;

        boolean finish = false;



        while(!finish){
            boolean success = true;
            int finishx = R+(2*S);
            int finishy = C+(2*S);
            int dim = (2*S)+1;


            for(int i = R; i<=finishx;i++) {
                for (int e = C; e <= finishy; e++) {
                    if(!isMatrixPoint(i,e) || picture[i][e] != '#'){
                        success =false;
                    }
                }
            }

            if(success){
                S++;
            }else{
                finish = true;
            }
        }

        return S-1;
    }

    //Este metodo soporta un posible fallo en el cuadrado que luego corregiremos con el ERASE_CELL
    public int findLongesSquareV3(int R, int C) {
        int S  = 1;
        int maxErrors =1;

          List<Integer> erraux = new ArrayList<Integer>();
        boolean finish = false;
        while(!finish){
            boolean success = true;
            int finishx = R+(2*S);
            int finishy = C+(2*S);
            int errors =0;


            for(int i = R; i<=finishx;i++) {
                for (int e = C; e <= finishy; e++) {
                    if(!isMatrixPoint(i,e)){
                        success =false;
                    }else if(picture[i][e] == '.' && errors<maxErrors){
                        errors ++;
                        erraux.add(i);
                        erraux.add(e);

                    }else if (errors>=maxErrors){
                        success =false;

                    }
                }
            }

            if(success){
                    for(int a = 0; a<erraux.size();a+=2){
                        err.add(erraux.get(a));
                        err.add(erraux.get(a+1));
                        picture[erraux.get(a)][erraux.get(a+1)]='E';
                    }
                    erraux.clear();
                     S++;
            }else{
                finish = true;
            }
        }

        return S-1;
    } //with one erro clear


    private boolean isMatrixPoint(int i, int e) {
        boolean result = false;
            if(i>0 && i<N && e>0 && e<M){
                result = true;
            }
        return result;
    }

    public void erasePoint(int x, int y){
        picture[x][y]='.';
    }

    public void paint(int startx,int starty, int finishx, int finishy,char a) {
        InstruccionNumber++;
       // System.out.println("punto inicial: "+startx+"-"+starty+"punto final: "+finishx+"-"+finishy);

        for (int i = startx; i <= finishx; i++) {
            for (int e = starty; e <= finishy; e++) {
                    picture[i][e] = a;
            }

        }
    }

    public int getInstruccionNumber() {
        return InstruccionNumber;
    }

    public void setInstruccionNumber(int instruccionNumber) {
        InstruccionNumber += instruccionNumber;
    }

    public List<Integer> getErr() {
        return err;
    }


    public  void compareResult(){
        int errors = 0;
        for(int i = 0; i<N;i++) {
            for (int e = 0; e < M; e++) {
                    if((pictureCopy[i][e]=='#'&& picture[i][e]=='*') ||(pictureCopy[i][e]=='.'&& picture[i][e]=='.')){

                }else{
                     //  System.out.println("ERRROR at["+i+"]["+e+"] Oringinal"+pictureCopy[i][e]+" Resultado"+picture[i][e]);
                        errors++;
                    }
             }
        }
        System.out.println("Errors: "+errors);

    }
}
